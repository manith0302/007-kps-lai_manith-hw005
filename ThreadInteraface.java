package thread.animation.run;

//Hello KSHRD
class HelloKshrd implements Runnable{
    String helloHrd = "Hello KSHRD!";
    @Override
    public void run() {
        for (int i=0; i<helloHrd.length() ;i++){
            System.out.print(helloHrd.charAt(i));
            try {
                Thread.sleep(300);
            }catch (Exception e){}
        }
    }
}

// Display ******
class DisplayStar implements Runnable{
    @Override
    public void run() {
        for (int j=0 ; j < 42 ;j++){
            System.out.print("*");
            try {
                Thread.sleep(300);
            }catch (Exception e){}
        }
    }
}

//I will try my best to be here, HRD Center.
class TryAtHrd implements Runnable{
    String tryToBeHrd = "I will try my best to be here, HRD Center.";
    @Override
    public void run() {
        for (int n=0 ; n < tryToBeHrd.length() ;n++){
            System.out.print(tryToBeHrd.charAt(n));
            try {
                Thread.sleep(300);
            }catch (Exception e){}
        }
    }
}

//DisplayMinus ------
class DisplayMinus implements Runnable{
    String tryToBeHrd = "I will try my best to be here, HRD Center.";
    @Override
    public void run() {
        for (int j=0 ; j < 42 ;j++){
            System.out.print("-");
            try {
                Thread.sleep(300);
            }catch (Exception e){}
        }
    }
}

//Download
class Download implements Runnable{
    String download = "Downloading..........";
    @Override
    public void run() {
        for (int j=0 ; j < download.length() ;j++){
            System.out.print(download.charAt(j));
            try {
                Thread.sleep(300);
            }catch (Exception e){}
        }
    }
}

//Completed
class Complete implements Runnable{
    String complete = "it's completed 100%";
    @Override
    public void run() {
        for (int j=0 ; j < complete.length() ;j++){
            System.out.print(complete.charAt(j));
            try {
                Thread.sleep(10);
            }catch (Exception e){}
        }
    }
}

public class ThreadInteraface {
    public static void main(String[] args) {
        Runnable task1 = new HelloKshrd();
        Runnable task2 = new DisplayStar();
        Runnable task3 = new TryAtHrd();
        Runnable task4 = new DisplayMinus();
        Runnable task5 = new Download();
        Runnable task6 = new Complete();

        Thread t1 = new Thread(task1);
        Thread t2 = new Thread(task2);
        Thread t3 = new Thread(task3);
        Thread t4 = new Thread(task4);
        Thread t5 = new Thread(task5);
        Thread t6 = new Thread(task6);

        try{Thread.sleep(500);}catch (Exception e){}
        t1.start();
        try{Thread.sleep(5000);}catch (Exception e){}
        System.out.println();
        t2.start();
        try{Thread.sleep(13000);}catch (Exception e){}
        System.out.println();
        t3.start();
        try{Thread.sleep(13000);}catch (Exception e){}
        System.out.println();
        t4.start();
        try{Thread.sleep(13000);}catch (Exception e){}
        System.out.println();
        t5.start();
        try{Thread.sleep(6500);}catch (Exception e){}
        t6.start();

    }
}
